---
layout: page
title: About
permalink: /about
section: about
---

SIGMΔ Alliance joined The Alliance in March 2017 as founding Member. Our aim is to provide a range of guilds under one roof for players all over the world, casual and hardcore and at any level. We create a fun an interactive group where you can grow as a player and make friends at the same time.

SIGMΔ Alliance prides ourselves on listening to all our members and having a great community build on friendships and fun. We started Guilds at every level in both the EU and USA timezones.
